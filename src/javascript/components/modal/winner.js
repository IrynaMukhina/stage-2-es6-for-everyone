import { showModal } from './modal';

export function showWinnerModal(fighter) {
  showModal({
    title: 'Game Over.',
    bodyElement: `Winer is ${fighter.name}`,
    onClose: () => console.log('The End')
  });
}
